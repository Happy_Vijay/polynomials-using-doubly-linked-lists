typedef struct node{
	int coef, exp;
	struct node *prev, *next;
}node;
typedef struct poly{
	struct node *head, *tail;
}poly;
/* Functions */
void init(poly *p);
void insert(poly *p, int coef, int exp);
poly add(poly *A, poly *B);
poly sub(poly *A, poly *B);
poly multiply(poly *A, poly *B);
void print(poly *A);
int length(poly *A);
