The polynomials uses doubly linked lists.
The node of list is
typedef struct node {
    int coeff,  exponent;
    node *next, *prev;
}node;

typedef struct polynomial {
    struct node *head, *tail;
}

Insertion:
The entries in polynomials are always kept in sorted form and non-duplicate, user need not worry , code takes care of adding elements in sorted form according 
to exponents.

Deletion of term:

Adding two nodes:
Since we have  two sorted list, we compare the exponents and add them one by one.
If two nodes have same exponent we add there coeff.
In this way we create a new polynomials by addng terms one by one.

Subtraction:
Similar logic to addition.

Mutiplication:
SLightly complicated