#include"poly.h"
#include<stdio.h>
#include<stdlib.h>
int main(){
	int choice;
	int coef, exp;
	poly D;
	poly A, B, C;
	init(&A);
	init(&B);
	init(&C);
	init(&D);
	while(1){
		printf("\n1. Init Polynomail\n2. Insert Entries\n3. Print Polynomial\n");
		printf("4. Addition\n 5. Subtraction\n 6. Multiplication\n7. Transpose\n8. Exit\n");
		scanf("%d", &choice);
		switch(choice){
		case 1	:
			break;
		case 2	:
			printf("Enter < c, e>\n");
			scanf("%d%d", &coef, &exp);
			insert(&D, coef, exp);
			break;
		case 3	:
			print(&D);
			break;
		case 4	:
			init(&A);
			init(&B);
			printf("Enter <c, e> entries for A: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&A, coef, exp);
			}
			printf("Enter <c, e> entries for B: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&B, coef, exp);
			}
			C = add(&A, &B);
			print(&C);
			break;
		case 5	:
			init(&A);
			init(&B);
			printf("Enter <c, e> entries for A: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&A, coef, exp);
			}
			printf("Enter <c, e> entries for B: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&B, coef, exp);
			}
			C = sub(&A, &B);
			print(&C);
			break;
		case 6	:
			init(&A);
			init(&B);
			printf("Enter <c, e> entries for A: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&A, coef, exp);
			}
			printf("Enter <c, e> entries for B: \n");
			while(scanf("%d%d", &coef, &exp) != EOF){
				insert(&B, coef, exp);
			}
			C = multiply(&A, &B);
			print(&C);
			break;
		case 8	:
			exit(0);
			break;
		default :
			printf("Enter Valid Choice\n");
		}	
	}
}
