#include"poly.h"
#include<stdio.h>
#include<stdlib.h>
#define COMPARE(a, b) ((a) > (b) ? 1 : (a) < (b) ? -1 : 0)
void init(poly *p){
	p->head = p -> tail = NULL;
}
poly add(poly *A, poly *B){
	poly C;
	node *p, *q;
	p = A->head;
	q = B->head;
	int c;
	init(&C);
	while(p && q){
		c = COMPARE(p->exp, q->exp);
		if(c == 0){
			insert(&C, p->coef + q->coef, p->exp);
			p = p->next;
			q = q->next;
		}
		else if(c < 0){
			insert(&C, p->coef, p->exp);
			p = p->next;
		}
		else{
			insert(&C, q->coef, q->exp);
			q = q->next;
		}
	}
	while(p && (q == NULL)){
		insert(&C, p->coef, p->exp);
		p = p->next;
	}
	while(q && (p == NULL)){
		insert(&C, q->coef, q->exp);
		q = q->next;
	}
	return C;
}
void print(poly *A){
	node *p = A->tail;
	int i, len = length(A);
	if(p == NULL){
		return;
	}
	else{
		for(i = 0; i < len - 1; i++){
			if(p->coef){
				printf("%dx^%d + ", p->coef, p->exp);
			}
			p = p->prev;
		}
		if(p->exp == 0){
			printf("%d\n", p->coef);
		}
		else{
			printf("%dx^%d\n", p->coef, p->exp);
		}
	}
}
poly multiply(poly *A, poly *B){
	node *p = A->head, *q;
	int e, c, l1, l2;
	int i, j;
	poly C;
	init(&C);
	l1 = length(A);
	l2 = length(B);
	for(i = 0; i < l1; i++){
		q = B->head;
		for(j = 0; j < l2; j++){
			e = p->exp + q->exp;
			c = p->coef * q->coef;
			insert(&C, c, e);
			q = q->next;
		}
		p = p->next;
	}
	return C;
}
int length(poly *A){
	int count = 0;
	node *p = A->head;
	while(p){
		p = p->next;
		count++;
	}
	return count;
}	
void insert(poly *p, int coef, int exp){
	node *q = p->head;
	node *temp;
	/* Create Node */
	temp = (node *)malloc(sizeof(node));
	if(!temp){
		return;
	}
	temp ->coef = coef;
	temp->exp = exp;
	/* First time insertion */
	if(p->head == NULL){
		p->head = p->tail = temp;
		temp->next = temp->prev = NULL;
		return;
	}
	/* Insertion at start */
	if(exp < p->head->exp){
		temp->prev = NULL;
		temp->next = p->head;
		p->head->prev = temp;
		p->head = temp;
		return;
	}
	/* Insertion at end */
	if(exp > p->tail->exp){
		temp->next = NULL;
		temp->prev = p->tail;
		p->tail->next = temp;
		p->tail = temp;
		return;
	}
	/* Searching */
	while(exp > q->exp){
		q = q->next;
	}
	if(exp == q->exp){
		free(temp);
		q->coef = coef + q->coef;
	}
	else{
		temp->next = q;
		temp->prev = q->prev;
		q->prev = temp;
		temp->prev->next = temp;
	}
}
poly sub(poly *A, poly *B){
	poly C;
	node *p, *q;
	p = A->head;
	q = B->head;
	int c;
	init(&C);
	while(p && q){
		c = COMPARE(p->exp, q->exp);
		if(c == 0){
			insert(&C, p->coef - q->coef, p->exp);
			p = p->next;
			q = q->next;
		}
		else if(c < 0){
			insert(&C, p->coef, p->exp);
			p = p->next;
		}
		else{
			insert(&C, (-1) * q->coef, q->exp);
			q = q->next;
		}
	}
	while(p && (q == NULL)){
		insert(&C, p->coef, p->exp);
		p = p->next;
	}
	while(q && (p == NULL)){
		insert(&C, (-1) * q->coef, q->exp);
		q = q->next;
	}
	return C;
}
